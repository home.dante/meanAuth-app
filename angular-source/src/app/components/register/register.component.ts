import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms'
import { FormBuilder } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
// import { FlashMessagesService } from 'angular2-flash-messages'
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  submitAttempt: boolean = false;
  userForm: FormGroup;
  errorMessage: any;

  constructor(public router: Router, public formBuilder: FormBuilder, private auth: AuthService) {
    this.initializeUserForm();
  }




  ngOnInit() {
  }


  onRegisterSubmit() {

    if (!this.userForm.valid) {
      this.submitAttempt = true;
      console.log("Invalid Data ", this.userForm.value)
      // alert('Invalid Data');
    }
    else {
      let userData = this.userForm.value;
      let userObject: userObjectInterface = {

        name: userData.name,
        username: userData.username,
        email: userData.email,
        password: userData.password,

      }
      //service request here
      console.log("userObject ", userObject);
      this.auth.registerUser(userObject).subscribe(data => {
        console.log(data);
        if (data.success) {

          this.router.navigate(['/login']);
        }
        else {
          this.errorMessage = data.err;
        }
      }, error => {
        console.log(error);
      });

    }
  }




  initializeUserForm() {
    this.userForm = this.formBuilder.group({
      name: [
        '', Validators.compose([
          Validators.maxLength(30),
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      email: [
        '', Validators.compose([
          Validators.maxLength(30),
          Validators.pattern("[^ @]*@[^ @]*"),
          Validators.required
        ])
      ],
      username: [
        '', Validators.compose([
          Validators.required, Validators.maxLength(30),
          Validators.minLength(3)
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.maxLength(30),
          Validators.minLength(8),
          Validators.required,
        ])
      ]

    });
  }
}
interface userObjectInterface {
  name: String;
  username: String;
  email: String;
  password: String;
}