import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit() {
    // console.log(this.auth.loggedIn())
  }

  onLogoutClick() {
    this.auth.logout().then((msg) => {
      console.log(msg);
    }, err => {

      console.log(err);
    })
  }

}
