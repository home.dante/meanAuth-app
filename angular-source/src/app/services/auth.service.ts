import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Http, Headers } from '@angular/http';
// import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtHelperService } from '@auth0/angular-jwt';
const helper = new JwtHelperService();
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authToken: any;
  user: any;

  constructor(
    // public jwtHelper: JwtHelperService,
    private http: HttpClient) { }
  root: String = '';

  registerUser(user): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json')
    return this.http.post(this.root + "users/register", user);

  }
  authenticateUser(user): any {
    return this.http.post(this.root + "users/authenticate", user);
  }
  getProfile(): any {
    this.loadToken();
    const headers: HttpHeaders = new HttpHeaders({
      'Authorization': JSON.parse(this.authToken)
    });
    console.log(this.authToken)
    // headers.append('Content-Type', 'application/json')
    // headers.append('Authorization', JSON.parse(this.authToken));
    return this.http.get(this.root + "users/profile", { headers: headers });

  }
  loadToken() {
    const token = localStorage.getItem("meanAuth-token");
    this.authToken = token;
  }
  storeUserData(token, user): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        localStorage.setItem("meanAuth-token", JSON.stringify(token));
        localStorage.setItem("meanAuth-user", JSON.stringify(user));
        this.user = user;
        this.authToken = token;
        resolve({ success: true, msg: "Authenticated" })
      } catch (err) {
        reject({ success: false, msg: err });
      }
    });

  }
  logout(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.user = null;
        this.authToken = null;
        localStorage.clear();
        resolve({ success: true, msg: "Logged Out" })
      } catch (err) {
        reject({ success: false, msg: err });
      }
    });
  }

  loggedIn() {
    // let tokenres = this.jwtHelper.isTokenExpired(JSON.parse(localStorage.getItem("meanAuth-token")));
    // return tokenres

    this.loadToken();
    if (this.authToken) {
      return !(helper.isTokenExpired(this.authToken));
    } else {
      return false;
    }


  }
}
