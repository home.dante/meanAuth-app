
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms'
import { FormBuilder } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  submitAttempt: boolean = false;
  userForm: FormGroup;
  errorMessage: any;

  constructor(public router: Router, public formBuilder: FormBuilder, private auth: AuthService) {
    this.initializeUserForm();
  }
  ngOnInit() {

  }
  onLoginSubmit() {

    if (!this.userForm.valid) {
      this.submitAttempt = true;
      // console.log("Invalid Data ", this.userForm.value)
      // alert('Invalid Data');
    }
    else {
      let userData = this.userForm.value;
      let userObject: userObjectInterface = {


        username: userData.username,
        // email: userData.email,
        password: userData.password,

      }
      //service request here
      // console.log("userObject ", userObject);
      this.auth.authenticateUser(userObject).subscribe(data => {
        if (data.success) {
          console.log("Logged in")
          this.auth.storeUserData(data.token, data.user).then((res) => {
            if (res.success)
              this.router.navigate(['/dashboard']);
            else {
              console.log(res.msg);

            }
          }, (err) => {

            console.log(err);
            this.errorMessage = err.msg;

          });

        }
        else {
          this.errorMessage = data.err;
        }
      }, error => {
        console.log(error);
      });

    }
  }




  initializeUserForm() {
    this.userForm = this.formBuilder.group({

      // email: [
      //   '', Validators.compose([
      //     Validators.maxLength(30),
      //     Validators.pattern("[^ @]*@[^ @]*"),
      //     Validators.required
      //   ])
      // ],
      username: [
        '', Validators.compose([
          Validators.required, Validators.maxLength(30),
          Validators.minLength(3)
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.maxLength(30),
          Validators.minLength(8),
          Validators.required,
        ])
      ]

    });
  }
}

interface userObjectInterface {

  username: String;
  // email: String;
  password: String;
}
