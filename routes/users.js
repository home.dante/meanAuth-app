const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken')
const User = require('../models/user')
const config = require('../config/database');
//Register
router.post('/register', (req, res, next) => {
    // res.send('REGISTER');
    let response = req.body;
    let newUser = new User({
        name: response.name,
        email: response.email,
        username: response.username,
        password: response.password
    });

    console.log(newUser)
    User.addUser(newUser, (err, user) => {
        if (err) {
            res.json({ success: false, msg: "Failed to register user" })
        }
        else {
            res.json({ success: true, msg: "User registered" })
        }
    })
});

//Authenticate
router.post('/authenticate', (req, res, next) => {
    //res.send('AUTHENTICATE');
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err, user) => {
        if (err) {
            throw (err)
        }
        if (!user) {
            return res.json({ success: false, msg: 'User not found' });
        }
        //
        User.comparePasswords(password, user.password, (err, isMatch) => {
            if (err) {
                throw (err)
            }
            if (isMatch) {
                // const token = "11";
                // console.log("OUR GUY ", JSON.parse(user))
                let userObject = {
                    _id: user._id,
                    name: user.name,
                    username: user.username,
                    email: user.email
                }
                const token = jwt.sign(userObject, config.secret,
                    {
                        expiresIn: 604800 //1 week
                    });

                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    user: userObject
                });
            }
            else {
                return res.json({ success: false, msg: 'Wrong password' });
            }
        });
        //
    });
});

//Profile
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {

    res.json({ user: req.user });
});



//Validate
// router.get('/validate', (req, res, next) => {
//     res.send('VALIDATE');
// });


module.exports = router;